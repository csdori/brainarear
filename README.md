BrainAreaR

The recent development in multielectrode array technology enables recording of potential with a high spatio-temporal resolution and to use more sophisticated experimental setups. There is a parallelly emerging need for more complex quantities describing the activity and spatial interaction of neuron population and visualization.We aim to target this demand by the BrainAreaR, a software with graphical user interface developed in R. Besides generating images of simple quantities as raw and filtered data, power and frequency spectrum, kernel source density distribution map and coherence is also calculated. One of the main features is the coherence-clustering which can be used for identifying functionally cohesive neuron population. This might be of special interest for those interested in changes of population level interactions under various paradigms. An application of this software will be demonstrated on simulated data of a 4x8 channel foli based multielectrode with 1 mm interelectrode distance. The result of the coherence clustering is a map that shows strong similarity to the underlying anatomical cortical area map.

4x8_1mmIED Channel: Best kCSD reconstruction is best in case of 0.5 basis width
4x8_0.5mmIED: Best kCSD reconstruction is best in case of 0.11 basis width


